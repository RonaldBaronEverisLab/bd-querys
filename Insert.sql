-- Schema: transaction

-- DROP SCHEMA transaction;

CREATE SCHEMA transaction
  AUTHORIZATION postgres;

INSERT INTO "transaction"."society" ("name", "code", "country", "currency", "language") VALUES
('Alicorp S.A.A.', 'PE11', 'PE', 'PEN', 'ES'), -- ID : 1
('Vitapro S.A.', 'PE12', 'PE', 'PEN', 'ES'), -- ID : 2
('Industrias Teal S.A.', 'PE13', 'PE', 'PEN', 'ES'), -- ID : 3
('Masterbread S.A.', 'PE14', 'PE', 'PEN', 'ES'), -- ID : 4
('Alicorp Inversiones S.A.', 'PE15', 'PE', 'PEN', 'ES'), -- ID : 5
('ProOriente S.A.', 'PE16', 'PE', 'PEN', 'ES'), -- ID : 6
('Consorcio Distribuidor Inq S.A.', 'PE17', 'PE', 'PEN', 'ES'), -- ID : 7
('R. Trading S.A.', 'PE18', 'PE', 'PEN', 'ES'), -- ID : 8
('Cernical Group S.A.', 'PE19', 'PE', 'PEN', 'ES'); -- ID : 9
SELECT *FROM transaction.society

INSERT INTO "transaction"."society_env_variable" ("society_id", "name", "value") VALUES
(1, 'TAXNR', '20100055237'),
(2, 'TAXNR', '20555271566'),
(3, 'TAXNR', '20100046831'),
(4, 'TAXNR', '20557345931'),
(5, 'TAXNR', '20543860361'),
(6, 'TAXNR', '20493645804'),
(7, 'TAXNR', '20103959251'),
(8, 'TAXNR', '20562926322'),
(9, 'TAXNR', '20602597947');
SELECT *FROM transaction.society_env_variable

INSERT INTO "transaction"."platform" ("description", "name") VALUES
('SAP Alicorp Peru',	'SAP', 1); -- ID: 1
('Banco de Crédito del Perú', 'BCP'); -- ID: 2
('Banco Interamericano de Finanzas', 'BANBIF'); -- ID:3
('Banco de la Nación', 'BANCO DE LA NACION'); -- ID:4
('Banco Pichincha', 'BANCO  PICHINCHA'); -- ID:5
('Bank of America', 'BANK OF AMERICA'); -- ID:6
('Banco Continental Perú', 'BBVA'), -- ID:7
('City Bank of New York','CITIBANK');--ID:8
('COMPASS GROUP', 'COMPASS  GROUP'),  --ID:9
('Banco Internacional del Perú, S.A.', 'INTERBANK'),  --ID:10
('Banco Santander Perú S.A.', 'BANCO SANTANDER'),    -- ID:11
('Banco Scotiabank Perú','SCOTIABANK');    --ID:12

SELECT *FROM transaction.platform


INSERT INTO "transaction"."operation"("code_name", "description", "name", "destination_id", "source_id", "operation_type") VALUES
('EB0001','Relacion de conciliaciones bancarias desde BCP hacia SAP Alicorp','Extractos Bancarios',1,2,'FILE_BASED');
('PG0004','Alicorp realiza los pagos desde SAP Alicorp hacia BCP','Pagos Confirming',2,1,'FILE_BASED');
('PG0002','Alicorp realiza los pagos a proveedores desde SAP Alicorp hacia BCP','Pagos Proveedores',,2,1,'FILE_BASED');
('RCOOO1','Pagos realizados por los clientes a una cuenta recaudadora de Alicorp desde SAP Alicorp hacia BCP','Recaudos  Batch con referencia',2,1,'FILE_BASED');
('RC0002','Pagos realizados por los clientes a una cuenta recaudadora de Alicorp desde SAP Alicorp hacia BCP','Recaudos  Batch sin referencia'2,1,'FILE_BASED');
('RL0001','Pagos realizados por los clientes a una cuenta recaudadora de Alicorp desde SAP Alicorp hacia BCP','Recaudos lista de clientes',2,1,'FILE_BASED');


SELECT *FROM transaction.operation

--No ejecuta
INSERT INTO "transaction"."operation_definition" ("column_order", "description", "field_type", "name", "operation_id") VALUES
(2,	NULL,	'KEYWORD',	'name',	1),
(1,	NULL,	'INTEGER',	'id',	1);
*/


INSERT INTO "transaction"."range" ("end_point", "size", "start_point") VALUES
(4,	5,	NULL),
(5,	5,	NULL);
SELECT *FROM transaction.range


--No ejecuta
INSERT INTO "transaction"."section" ("range_id") VALUES
(1),
(2);

--No ejecuta
INSERT INTO "transaction"."file_setting" ("column_separator", "encoding", "line_separator", "splitter_type", "type", "footer_id", "header_id") VALUES
(',',	'utf8',	'',	'line',	'text/x-csv',	NULL,	1),
(',',	'utf8',	'',	'line',	'text/x-csv',	NULL,	2);


INSERT INTO "transaction"."step_parameter" ("input_type", "output_type", "rules") VALUES
('text/x-csv',	'application/json+transaction-json',	'[{"source": {"type": "timestamp", "column": 1, "format": "dd/MM/yyyy HH:mm:ss"}, "destination": {"type": "timestamp", "field": "process-date"}}, {"source": {"column": 2}, "destination": {"field": "name"}}, {"source": {"column": 3}, "destination": {"field": "lastname"}}, {"source": {"column": 4}, "destination": {"field": "company.code"}}]');
SELECT *FROM transaction.step_parameter


--No ejecuta
INSERT INTO "file_based_transaction_setting" ("input_file_setting_id", "output_file_setting_id", "operation_id") VALUES
(1,	2,	1);

--No ejecuta
INSERT INTO "step_setting" ("async", "name", "resolver", "parameter_id", "transaction_setting_id") VALUES
(NULL,	'from-bcp-to-json',	'parser',	1,	1);



--No ejecuta
INSERT INTO "transaction"."operation_env_variable" ("operation_id", "name", "value", "description") VALUES
(1,	'EXT_BANC_VARIABLE',	'Custom Variable',	'Variable de Prueba');



--No ejecuta
INSERT INTO "platform_env_variable" ("platform_id", "name", "value", "description") VALUES
(2,	'BCP_ENDPOINT',	'https://endpoints.lima.bcp.com.pe/api/v1',	'Endpoint para BCP');












