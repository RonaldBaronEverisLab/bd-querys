-- Schema: transaction

-- DROP SCHEMA transaction;

CREATE SCHEMA transaction
  AUTHORIZATION postgres;

SELECT *FROM transaction.society
SELECT *FROM transaction.society_env_variable
SELECT *FROM transaction.platform
SELECT *FROM transaction.platform_env_variable
SELECT *FROM transaction.operation 
SELECT *FROM transaction.operation_definition
SELECT *FROM transaction.operation_env_variable 
SELECT *FROM transaction.range
SELECT *FROM transaction.step_parameter
SELECT *FROM transaction.file_setting 
SELECT *FROM transaction.file_based_transaction_setting 
SELECT *FROM transaction.step_setting
SELECT *FROM transaction.transaction_status 
SELECT *FROM transaction.base_transaction
SELECT *FROM transaction.inner_transaction 