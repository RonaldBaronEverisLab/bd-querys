--Schema: transaction

-- DROP SCHEMA transaction;

CREATE SCHEMA transaction
  AUTHORIZATION postgres;


DROP TABLE IF EXISTS "inner_transaction" CASCADE;
DROP TABLE IF EXISTS "base_transaction" CASCADE;
DROP TABLE IF EXISTS "transaction_status" CASCADE;
DROP TABLE IF EXISTS "file_based_transaction_setting" CASCADE;
DROP TABLE IF EXISTS "file_setting" CASCADE;
DROP TABLE IF EXISTS "step_setting" CASCADE;
DROP TABLE IF EXISTS "step_parameter" CASCADE;
DROP TABLE IF EXISTS "section" CASCADE;
DROP TABLE IF EXISTS "range" CASCADE;
DROP TABLE IF EXISTS "operation_env_variable";
DROP TABLE IF EXISTS "operation_definition" CASCADE;
DROP TABLE IF EXISTS "operation" CASCADE;
DROP TABLE IF EXISTS "platform_env_variable";
DROP TABLE IF EXISTS "platform" CASCADE;
DROP TABLE IF EXISTS "society_env_variable" CASCADE;
DROP TABLE IF EXISTS "society" CASCADE;

DROP TYPE  IF EXISTS "operation_type";
DROP TYPE  IF EXISTS "field_type";
DROP TYPE  IF EXISTS "currency";
DROP TYPE  IF EXISTS "language_abbrv";

DROP SEQUENCE IF EXISTS file_based_transaction_setting_id_seq;
CREATE SEQUENCE file_based_transaction_setting_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS file_setting_id_seq;
CREATE SEQUENCE file_setting_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS operation_id_seq;
CREATE SEQUENCE operation_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS operation_definition_id_seq;
CREATE SEQUENCE operation_definition_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS operation_env_variable_id_seq;
CREATE SEQUENCE operation_env_variable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS platform_id_seq;
CREATE SEQUENCE platform_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS platform_env_variable_id_seq;
CREATE SEQUENCE platform_env_variable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS society_id_seq;
CREATE SEQUENCE society_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS society_env_variable_id_seq;
CREATE SEQUENCE society_env_variable_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS range_id_seq;
CREATE SEQUENCE range_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS section_id_seq;
CREATE SEQUENCE section_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS step_parameter_id_seq;
CREATE SEQUENCE step_parameter_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS step_setting_id_seq;
CREATE SEQUENCE step_setting_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;

DROP SEQUENCE IF EXISTS transaction_status_id_seq;
CREATE SEQUENCE transaction_status_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START 1 CACHE 1;




CREATE TYPE   "operation_type" AS ENUM (
    'FILE_BASED',
    'SYNC',
    'ASYNC'
);

CREATE TYPE   "field_type" AS ENUM (
    'BOOLEAN',
    'KEYWORD',
    'TEXT',
    'DATE',
    'LONG',
    'INTEGER',
    'SHORT',
    'BYTE',
    'DOUBLE',
    'FLOAT',
    'HALF_FLOAT',
    'SCALED_FLOAT',
    'OBJECT',
    'GEO_POINT',
    'IP'
);

-- Source: https://www.currency-iso.org/en/home/tables/table-a1.html
CREATE TYPE   "currency" AS ENUM (
    'PEN', -- Sol
    'USD', -- US Dollar
    'EUR', -- Euro
    'UYU', -- Uruguay Peso
    'ARS', -- Argentine Peso
    'CLP', -- Chilean Peso
    'BRL', -- Brazilian Real
    'COP', -- Colombian Peso
    'VES', -- Bolívar Soberano
    'PYG', -- Guarani
    'BOB', -- Boliviano
    'MXN', -- Mexican Peso
    'CAD', -- Canadian Dollar
    'CNY', -- (China) Yuan Renminbi
    'JPY', -- (Japan) Yen
    'CRC', -- Costa Rican Colon
    'CUP', -- Cuban Peso
    'DOP', -- Dominican Peso
    'SVC', -- El Salvador Colon
    'GTQ', -- (Guatemala) Quetzal
    'HNL', -- (Honduras) Lempira
    'NIO'  -- (Nicaragua) Cordoba Oro
);

CREATE TYPE   "language_abbrv" AS ENUM (
    'ES',
    'EN',
    'PT',
    'FR',
    'DE',
    'IT'
);


CREATE TABLE   transaction.society (
    "id" integer DEFAULT nextval('society_id_seq') NOT NULL,
    "code" character varying(10) NOT NULL,
    "name" character varying(100) NOT NULL,
    "description" text,
    "currency" currency NOT NULL,
    "language" language_abbrv NOT NULL,
    "country" character varying(10) NOT NULL,
    CONSTRAINT "society_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_society_code" UNIQUE ("code"),
    CONSTRAINT "unique_society_name" UNIQUE ("name")
) WITH (oids = false);

SELECT *FROM  "transaction"."society";

CREATE TABLE   transaction.society_env_variable(
    "id" integer DEFAULT nextval('society_env_variable_id_seq') NOT NULL,
    "society_id" integer NOT NULL,
    "name" character varying(255) NOT NULL,
    "value" text NOT NULL,
    "description" text,
    CONSTRAINT "society_env_variable_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_society_env_variable__society" FOREIGN KEY (society_id) REFERENCES society(id) NOT DEFERRABLE
) WITH (oids = false);


SELECT *FROM  "transaction"."society_env_variable";


CREATE TABLE   transaction.platform(
    "id" integer DEFAULT nextval('platform_id_seq') NOT NULL,
    "name" character varying(100) NOT NULL,
    "description" text,
    "society_id" integer,
    CONSTRAINT "platform_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_platform__society" FOREIGN KEY (society_id) REFERENCES society(id) NOT DEFERRABLE
) WITH (oids = false);

SELECT *FROM "transaction"."platform";




CREATE TABLE   transaction.platform_env_variable (
    "id" integer DEFAULT nextval('platform_env_variable_id_seq') NOT NULL,
    "platform_id" integer NOT NULL,
    "name" character varying(255) NOT NULL,
    "value" text NOT NULL,
    "description" text,
    CONSTRAINT "platform_env_variable_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_platform_env_variable__platform" FOREIGN KEY (platform_id) REFERENCES platform(id) NOT DEFERRABLE
) WITH (oids = false);

SELECT *FROM "transaction"."platform_env_variable";




CREATE TABLE   transaction.operation (
    "id" integer DEFAULT nextval('operation_id_seq') NOT NULL,
    "code_name" character varying(15) NOT NULL,
    "description" text,
    "name" character varying(100) NOT NULL,
    "destination_id" integer NOT NULL,
    "source_id" integer NOT NULL,
    "operation_type" operation_type NOT NULL,
    CONSTRAINT "operation_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "unique_operation_code_name" UNIQUE ("code_name"),
    CONSTRAINT "fkey_operation__platform__destination_id" FOREIGN KEY (destination_id) REFERENCES platform(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_operation__platform__source_id" FOREIGN KEY (source_id) REFERENCES platform(id) NOT DEFERRABLE
) WITH (oids = false);

SELECT *FROM "transaction"."operation";
DROP TABLE transaction.operation;


CREATE TABLE  transaction.operation_definition(
    "id" integer DEFAULT nextval('operation_definition_id_seq') NOT NULL,
    "column_order" integer NOT NULL,
    "description" text,
    "name" character varying(100) NOT NULL,
    "field_type" field_type NOT NULL,
    "format" character varying(100),
    "reference_field" character varying(100),
    "operation_id" integer NOT NULL,
    CONSTRAINT "operation_definition_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_operation_definition__operation" FOREIGN KEY (operation_id) REFERENCES operation(id) NOT DEFERRABLE
) WITH (oids = false);


CREATE TABLE   transaction.operation_env_variable (
    "id" integer DEFAULT nextval('operation_env_variable_id_seq') NOT NULL,
    "operation_id" integer NOT NULL,
    "name" character varying(100) NOT NULL,
    "value" text NOT NULL,
    "description" text,
    CONSTRAINT "operation_env_variable_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_operation_env_variable__operation" FOREIGN KEY (operation_id) REFERENCES operation(id) NOT DEFERRABLE
) WITH (oids = false);


CREATE TABLE   transaction.range (
    "id" integer DEFAULT nextval('range_id_seq') NOT NULL,
    "end_point" integer,
    "size" integer NOT NULL,
    "start_point" integer,
    CONSTRAINT "range_pkey" PRIMARY KEY ("id")
) WITH (oids = false);





CREATE TABLE   transaction.section (
    "id" integer DEFAULT nextval('section_id_seq') NOT NULL,
    "range_id" integer NOT NULL,
    CONSTRAINT "section_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_section__range" FOREIGN KEY (range_id) REFERENCES range(id) NOT DEFERRABLE
) WITH (oids = false);



CREATE TABLE   transaction.step_parameter(
    "id" integer DEFAULT nextval('step_parameter_id_seq') NOT NULL,
    "input_type" character varying(100) NOT NULL,
    "output_type" character varying(100),
    "rules" jsonb,
    CONSTRAINT "step_parameter_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

SELECT *FROM "transaction"."step_parameter";



CREATE TABLE   transaction.file_setting (
    "id" integer DEFAULT nextval('file_setting_id_seq') NOT NULL,
    "column_separator" character varying(10),
    "encoding" character varying(10) NOT NULL,
    "line_separator" character varying(10),
    "splitter_type" character varying(10),
    "type" character varying(50) NOT NULL,
    "footer_id" integer,
    "header_id" integer,
    CONSTRAINT "file_setting_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_file_setting__section__header_id" FOREIGN KEY (header_id) REFERENCES section(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_file_setting__section__footer_id" FOREIGN KEY (footer_id) REFERENCES section(id) NOT DEFERRABLE
) WITH (oids = false);*/


CREATE TABLE   transaction.file_based_transaction_setting (
    "id" integer DEFAULT nextval('file_based_transaction_setting_id_seq') NOT NULL,
    "input_file_setting_id" integer NOT NULL,
    "output_file_setting_id" integer NOT NULL,
    "operation_id" integer NOT NULL,
    CONSTRAINT "file_based_transaction_setting_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_file_based_tx_setting__operation" FOREIGN KEY (operation_id) REFERENCES operation(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_file_based_tx_setting__file_setting_input" FOREIGN KEY (input_file_setting_id) REFERENCES file_setting(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_file_based_tx_setting__file_setting_output" FOREIGN KEY (output_file_setting_id) REFERENCES file_setting(id) NOT DEFERRABLE
) WITH (oids = false);*/


CREATE TABLE   transaction.step_setting (
    "id" integer DEFAULT nextval('step_setting_id_seq') NOT NULL,
    "async" boolean,
    "name" character varying(100) NOT NULL,
    "resolver" character varying(50) NOT NULL,
    "parameter_id" integer NOT NULL,
    "transaction_setting_id" integer NOT NULL,
    CONSTRAINT "step_setting_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_step_setting__step_parameter" FOREIGN KEY (parameter_id) REFERENCES step_parameter(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_step_setting__file_based_transaction_setting" FOREIGN KEY (transaction_setting_id) REFERENCES file_based_transaction_setting(id) NOT DEFERRABLE
) WITH (oids = false);*/


CREATE TABLE   transaction.transaction_status (
    "id" integer DEFAULT nextval('transaction_status_id_seq') NOT NULL,
    "description" text,
    "name" character varying(100) NOT NULL,
    CONSTRAINT "transaction_status_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

SELECT *FROM "transaction"."transaction_status";


CREATE TABLE   transaction.base_transaction (
    "id" uuid NOT NULL,
    "creation_date" timestamp NOT NULL,
    "departure_date" timestamp,
    "description" text,
    "destination_link" text,
    "full_username" character varying(255),
    "has_processed_file" boolean NOT NULL,
    "has_raw_file" boolean NOT NULL,
    "order_date" timestamp,
    "source_link" text,
    "status_message" text,
    "username" character varying(50) NOT NULL,
    "operation_id" integer NOT NULL,
    "status_id" integer NOT NULL,
    CONSTRAINT "base_transaction_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_base_transaction__operation" FOREIGN KEY (operation_id) REFERENCES operation(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_base_transaction__transaction_status" FOREIGN KEY (status_id) REFERENCES transaction_status(id) NOT DEFERRABLE
) WITH (oids = false);*/


CREATE TABLE   transaction.inner_transaction (
    "id" uuid NOT NULL,
    "creation_date" timestamp NOT NULL,
    "departure_date" timestamp,
    "input_raw_content" text,
    "output_raw_content" text,
    "parsed_content" jsonb,
    "transformed_content" jsonb,
    "parent_transaction_id" uuid,
    "status_id" integer NOT NULL,
    CONSTRAINT "inner_transaction_pkey" PRIMARY KEY ("id"),
    CONSTRAINT "fkey_inner_transaction__base_transaction" FOREIGN KEY (parent_transaction_id) REFERENCES base_transaction(id) NOT DEFERRABLE,
    CONSTRAINT "fkey_inner_transaction__transaction_status" FOREIGN KEY (status_id) REFERENCES transaction_status(id) NOT DEFERRABLE
) WITH (oids = false);*/

-- 2019-10-27 18:57:09.605257+00


























