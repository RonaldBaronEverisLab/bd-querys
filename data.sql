INSERT INTO "society" ("name", "code", "country", "currency", "language") VALUES
('Alicorp S.A.A.', 'PE11', 'PE', 'PEN', 'ES'), -- ID : 1
('Vitapro S.A.', 'PE12', 'PE', 'PEN', 'ES'), -- ID : 2
('Industrias Teal S.A.', 'PE13', 'PE', 'PEN', 'ES'), -- ID : 3
('Masterbread S.A.', 'PE14', 'PE', 'PEN', 'ES'), -- ID : 4
('Alicorp Inversiones S.A.', 'PE15', 'PE', 'PEN', 'ES'), -- ID : 5
('ProOriente S.A.', 'PE16', 'PE', 'PEN', 'ES'), -- ID : 6
('Consorcio Distribuidor Inq S.A.', 'PE17', 'PE', 'PEN', 'ES'), -- ID : 7
('R. Trading S.A.', 'PE18', 'PE', 'PEN', 'ES'), -- ID : 8
('Cernical Group S.A.', 'PE19', 'PE', 'PEN', 'ES'); -- ID : 9

INSERT INTO "society_env_variable" ("society_id", "name", "value") VALUES
(1, 'TAXNR', '20100055237'),
(2, 'TAXNR', '20555271566'),
(3, 'TAXNR', '20100046831'),
(4, 'TAXNR', '20557345931'),
(5, 'TAXNR', '20543860361'),
(6, 'TAXNR', '20493645804'),
(7, 'TAXNR', '20103959251'),
(8, 'TAXNR', '20562926322'),
(9, 'TAXNR', '20602597947');

INSERT INTO "platform" ("description", "name", "society_id") VALUES
('SAP Alicorp Peru',	'SAP', 1); -- ID: 1
INSERT INTO "platform" ("description", "name") VALUES
('Banco de Crédito del Perú', 'BCP'); -- ID: 2


INSERT INTO "operation" ("code_name", "description", "name", "destination_id", "source_id", "operation_type") VALUES
('EB0001',	'Relacion de conciliaciones bancarias desde BCP hacia SAP Alicorp',	'Extractos Bancarios',	2,	1,	'FILE_BASED'); -- ID: 1


INSERT INTO "operation_definition" ("column_order", "description", "field_type", "name", "operation_id") VALUES
(2,	NULL,	'KEYWORD',	'name',	1),
(1,	NULL,	'INTEGER',	'id',	1);

INSERT INTO "range" ("end_point", "size", "start_point") VALUES
(4,	5,	NULL),
(5,	5,	NULL);

INSERT INTO "section" ("range_id") VALUES
(1),
(2);

INSERT INTO "file_setting" ("column_separator", "encoding", "line_separator", "splitter_type", "type", "footer_id", "header_id") VALUES
(',',	'utf8',	'',	'line',	'text/x-csv',	NULL,	1),
(',',	'utf8',	'',	'line',	'text/x-csv',	NULL,	2);

INSERT INTO "step_parameter" ("input_type", "output_type", "rules") VALUES
('text/x-csv',	'application/json+transaction-json',	'[{"source": {"type": "timestamp", "column": 1, "format": "dd/MM/yyyy HH:mm:ss"}, "destination": {"type": "timestamp", "field": "process-date"}}, {"source": {"column": 2}, "destination": {"field": "name"}}, {"source": {"column": 3}, "destination": {"field": "lastname"}}, {"source": {"column": 4}, "destination": {"field": "company.code"}}]');

INSERT INTO "file_based_transaction_setting" ("input_file_setting_id", "output_file_setting_id", "operation_id") VALUES
(1,	2,	1);

INSERT INTO "step_setting" ("async", "name", "resolver", "parameter_id", "transaction_setting_id") VALUES
(NULL,	'from-bcp-to-json',	'parser',	1,	1);

INSERT INTO "operation_env_variable" ("operation_id", "name", "value", "description") VALUES
(1,	'EXT_BANC_VARIABLE',	'Custom Variable',	'Variable de Prueba');

INSERT INTO "platform_env_variable" ("platform_id", "name", "value", "description") VALUES
(2,	'BCP_ENDPOINT',	'https://endpoints.lima.bcp.com.pe/api/v1',	'Endpoint para BCP');