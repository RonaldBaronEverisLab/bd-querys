--
-- PostgreSQL database dump
--

-- Dumped from database version 10.10
-- Dumped by pg_dump version 10.10 (Ubuntu 10.10-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: transaction2; Type: SCHEMA; Schema: -; Owner: admincloud
--

CREATE SCHEMA transaction2;


ALTER SCHEMA transaction2 OWNER TO admincloud;

--
-- Name: field_type; Type: TYPE; Schema: transaction2; Owner: admincloud
--

CREATE TYPE transaction2.field_type AS ENUM (
    'BOOLEAN',
    'KEYWORD',
    'TEXT',
    'DATE',
    'LONG',
    'INTEGER',
    'SHORT',
    'BYTE',
    'DOUBLE',
    'FLOAT',
    'HALF_FLOAT',
    'SCALED_FLOAT',
    'OBJECT',
    'GEO_POINT',
    'IP'
);


ALTER TYPE transaction2.field_type OWNER TO admincloud;

--
-- Name: operation_type; Type: TYPE; Schema: transaction2; Owner: admincloud
--

CREATE TYPE transaction2.operation_type AS ENUM (
    'FILE_BASED',
    'SYNC',
    'ASYNC'
);


ALTER TYPE transaction2.operation_type OWNER TO admincloud;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: base_transaction; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.base_transaction (
    id uuid NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    departure_date timestamp without time zone,
    description text,
    destination_link text,
    full_username character varying(255),
    has_processed_file boolean NOT NULL,
    has_raw_file boolean NOT NULL,
    order_date timestamp without time zone,
    source_link text,
    status_message text,
    username character varying(255) NOT NULL,
    operation_id integer NOT NULL,
    status_id integer NOT NULL
);


ALTER TABLE transaction2.base_transaction OWNER TO admincloud;

--
-- Name: file_based_transaction_setting_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.file_based_transaction_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.file_based_transaction_setting_id_seq OWNER TO admincloud;

--
-- Name: file_based_transaction_setting; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.file_based_transaction_setting (
    id integer DEFAULT nextval('transaction2.file_based_transaction_setting_id_seq'::regclass) NOT NULL,
    input_file_setting_id integer NOT NULL,
    output_file_setting_id integer NOT NULL,
    operation_id integer NOT NULL
);


ALTER TABLE transaction2.file_based_transaction_setting OWNER TO admincloud;

--
-- Name: file_setting_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.file_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.file_setting_id_seq OWNER TO admincloud;

--
-- Name: file_setting; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.file_setting (
    id integer DEFAULT nextval('transaction2.file_setting_id_seq'::regclass) NOT NULL,
    column_separator character varying(255),
    encoding character varying(255) NOT NULL,
    line_separator character varying(255),
    splitter_type character varying(255),
    type character varying(255) NOT NULL,
    footer_id integer,
    header_id integer
);


ALTER TABLE transaction2.file_setting OWNER TO admincloud;

--
-- Name: inner_transaction; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.inner_transaction (
    id uuid NOT NULL,
    creation_date timestamp without time zone NOT NULL,
    departure_date timestamp without time zone,
    input_raw_content text,
    output_raw_content text,
    parsed_content jsonb,
    transformed_content jsonb,
    parent_transaction_id uuid,
    status_id integer NOT NULL
);


ALTER TABLE transaction2.inner_transaction OWNER TO admincloud;

--
-- Name: operation_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.operation_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.operation_id_seq OWNER TO admincloud;

--
-- Name: operation; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.operation (
    id integer DEFAULT nextval('transaction2.operation_id_seq'::regclass) NOT NULL,
    code_name character varying(255) NOT NULL,
    description text,
    name character varying(255) NOT NULL,
    destination_id integer NOT NULL,
    source_id integer NOT NULL,
    operation_type transaction2.operation_type NOT NULL
);


ALTER TABLE transaction2.operation OWNER TO admincloud;

--
-- Name: operation_definition_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.operation_definition_id_seq
    START WITH 3
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.operation_definition_id_seq OWNER TO admincloud;

--
-- Name: operation_definition; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.operation_definition (
    id integer DEFAULT nextval('transaction2.operation_definition_id_seq'::regclass) NOT NULL,
    column_order integer NOT NULL,
    description text,
    format character varying(255),
    name character varying(255) NOT NULL,
    field_type transaction2.field_type NOT NULL,
    operation_id integer NOT NULL
);


ALTER TABLE transaction2.operation_definition OWNER TO admincloud;

--
-- Name: operation_env_variable_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.operation_env_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.operation_env_variable_id_seq OWNER TO admincloud;

--
-- Name: operation_env_variable; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.operation_env_variable (
    id integer DEFAULT nextval('transaction2.operation_env_variable_id_seq'::regclass) NOT NULL,
    operation_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    description text
);


ALTER TABLE transaction2.operation_env_variable OWNER TO admincloud;

--
-- Name: platform_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.platform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.platform_id_seq OWNER TO admincloud;

--
-- Name: platform; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.platform (
    id integer DEFAULT nextval('transaction2.platform_id_seq'::regclass) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE transaction2.platform OWNER TO admincloud;

--
-- Name: platform_env_variable_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.platform_env_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.platform_env_variable_id_seq OWNER TO admincloud;

--
-- Name: platform_env_variable; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.platform_env_variable (
    id integer DEFAULT nextval('transaction2.platform_env_variable_id_seq'::regclass) NOT NULL,
    platform_id integer NOT NULL,
    name character varying(255) NOT NULL,
    value text,
    description text
);


ALTER TABLE transaction2.platform_env_variable OWNER TO admincloud;

--
-- Name: range_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.range_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.range_id_seq OWNER TO admincloud;

--
-- Name: range; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.range (
    id integer DEFAULT nextval('transaction2.range_id_seq'::regclass) NOT NULL,
    end_point integer,
    size integer NOT NULL,
    start_point integer
);


ALTER TABLE transaction2.range OWNER TO admincloud;

--
-- Name: section_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.section_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.section_id_seq OWNER TO admincloud;

--
-- Name: section; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.section (
    id integer DEFAULT nextval('transaction2.section_id_seq'::regclass) NOT NULL,
    range_id integer NOT NULL
);


ALTER TABLE transaction2.section OWNER TO admincloud;

--
-- Name: step_parameter_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.step_parameter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.step_parameter_id_seq OWNER TO admincloud;

--
-- Name: step_parameter; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.step_parameter (
    id integer DEFAULT nextval('transaction2.step_parameter_id_seq'::regclass) NOT NULL,
    input_type character varying(255) NOT NULL,
    output_type character varying(255),
    rules jsonb
);


ALTER TABLE transaction2.step_parameter OWNER TO admincloud;

--
-- Name: step_setting_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.step_setting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.step_setting_id_seq OWNER TO admincloud;

--
-- Name: step_setting; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.step_setting (
    id integer DEFAULT nextval('transaction2.step_setting_id_seq'::regclass) NOT NULL,
    async boolean,
    name character varying(255) NOT NULL,
    resolver character varying(255) NOT NULL,
    parameter_id integer NOT NULL,
    transaction_setting_id integer NOT NULL
);


ALTER TABLE transaction2.step_setting OWNER TO admincloud;

--
-- Name: transaction_status_id_seq; Type: SEQUENCE; Schema: transaction2; Owner: admincloud
--

CREATE SEQUENCE transaction2.transaction_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE transaction2.transaction_status_id_seq OWNER TO admincloud;

--
-- Name: transaction_status; Type: TABLE; Schema: transaction2; Owner: admincloud
--

CREATE TABLE transaction2.transaction_status (
    id integer DEFAULT nextval('transaction2.transaction_status_id_seq'::regclass) NOT NULL,
    description text,
    name character varying(255) NOT NULL
);


ALTER TABLE transaction2.transaction_status OWNER TO admincloud;

--
-- Data for Name: base_transaction; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.base_transaction (id, creation_date, departure_date, description, destination_link, full_username, has_processed_file, has_raw_file, order_date, source_link, status_message, username, operation_id, status_id) FROM stdin;
\.


--
-- Data for Name: file_based_transaction_setting; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.file_based_transaction_setting (id, input_file_setting_id, output_file_setting_id, operation_id) FROM stdin;
\.


--
-- Data for Name: file_setting; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.file_setting (id, column_separator, encoding, line_separator, splitter_type, type, footer_id, header_id) FROM stdin;
\.


--
-- Data for Name: inner_transaction; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.inner_transaction (id, creation_date, departure_date, input_raw_content, output_raw_content, parsed_content, transformed_content, parent_transaction_id, status_id) FROM stdin;
\.


--
-- Data for Name: operation; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.operation (id, code_name, description, name, destination_id, source_id, operation_type) FROM stdin;
\.


--
-- Data for Name: operation_definition; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.operation_definition (id, column_order, description, format, name, field_type, operation_id) FROM stdin;
\.


--
-- Data for Name: operation_env_variable; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.operation_env_variable (id, operation_id, name, value, description) FROM stdin;
\.


--
-- Data for Name: platform; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.platform (id, description, name) FROM stdin;
1	SAP Alicorp Peru	SAP
3	Banco de Crédito del Perú	BCP
4	prueba	prueba
\.


--
-- Data for Name: platform_env_variable; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.platform_env_variable (id, platform_id, name, value, description) FROM stdin;
\.


--
-- Data for Name: range; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.range (id, end_point, size, start_point) FROM stdin;
\.


--
-- Data for Name: section; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.section (id, range_id) FROM stdin;
\.


--
-- Data for Name: step_parameter; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.step_parameter (id, input_type, output_type, rules) FROM stdin;
\.


--
-- Data for Name: step_setting; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.step_setting (id, async, name, resolver, parameter_id, transaction_setting_id) FROM stdin;
\.


--
-- Data for Name: transaction_status; Type: TABLE DATA; Schema: transaction2; Owner: admincloud
--

COPY transaction2.transaction_status (id, description, name) FROM stdin;
\.


--
-- Name: file_based_transaction_setting_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.file_based_transaction_setting_id_seq', 1, false);


--
-- Name: file_setting_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.file_setting_id_seq', 1, false);


--
-- Name: operation_definition_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.operation_definition_id_seq', 3, false);


--
-- Name: operation_env_variable_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.operation_env_variable_id_seq', 1, false);


--
-- Name: operation_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.operation_id_seq', 3, false);


--
-- Name: platform_env_variable_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.platform_env_variable_id_seq', 1, false);


--
-- Name: platform_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.platform_id_seq', 4, true);


--
-- Name: range_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.range_id_seq', 1, false);


--
-- Name: section_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.section_id_seq', 1, false);


--
-- Name: step_parameter_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.step_parameter_id_seq', 1, false);


--
-- Name: step_setting_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.step_setting_id_seq', 1, false);


--
-- Name: transaction_status_id_seq; Type: SEQUENCE SET; Schema: transaction2; Owner: admincloud
--

SELECT pg_catalog.setval('transaction2.transaction_status_id_seq', 1, false);


--
-- Name: base_transaction base_transaction_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.base_transaction
    ADD CONSTRAINT base_transaction_pkey PRIMARY KEY (id);


--
-- Name: file_based_transaction_setting file_based_transaction_setting_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_based_transaction_setting
    ADD CONSTRAINT file_based_transaction_setting_pkey PRIMARY KEY (id);


--
-- Name: file_setting file_setting_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_setting
    ADD CONSTRAINT file_setting_pkey PRIMARY KEY (id);


--
-- Name: inner_transaction inner_transaction_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.inner_transaction
    ADD CONSTRAINT inner_transaction_pkey PRIMARY KEY (id);


--
-- Name: operation_definition operation_definition_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation_definition
    ADD CONSTRAINT operation_definition_pkey PRIMARY KEY (id);


--
-- Name: operation_env_variable operation_env_variable_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation_env_variable
    ADD CONSTRAINT operation_env_variable_pkey PRIMARY KEY (id);


--
-- Name: operation operation_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation
    ADD CONSTRAINT operation_pkey PRIMARY KEY (id);


--
-- Name: platform_env_variable platform_env_variable_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.platform_env_variable
    ADD CONSTRAINT platform_env_variable_pkey PRIMARY KEY (id);


--
-- Name: platform platform_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (id);


--
-- Name: range range_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.range
    ADD CONSTRAINT range_pkey PRIMARY KEY (id);


--
-- Name: section section_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.section
    ADD CONSTRAINT section_pkey PRIMARY KEY (id);


--
-- Name: step_parameter step_parameter_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.step_parameter
    ADD CONSTRAINT step_parameter_pkey PRIMARY KEY (id);


--
-- Name: step_setting step_setting_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.step_setting
    ADD CONSTRAINT step_setting_pkey PRIMARY KEY (id);


--
-- Name: transaction_status transaction_status_pkey; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.transaction_status
    ADD CONSTRAINT transaction_status_pkey PRIMARY KEY (id);


--
-- Name: operation unique_operation_code_name; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation
    ADD CONSTRAINT unique_operation_code_name UNIQUE (code_name);


--
-- Name: operation_env_variable unique_operation_env_variable_name; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation_env_variable
    ADD CONSTRAINT unique_operation_env_variable_name UNIQUE (name);


--
-- Name: platform_env_variable unique_platform_env_variable_name; Type: CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.platform_env_variable
    ADD CONSTRAINT unique_platform_env_variable_name UNIQUE (name);


--
-- Name: operation_definition feky_operation_definition__operation; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation_definition
    ADD CONSTRAINT feky_operation_definition__operation FOREIGN KEY (operation_id) REFERENCES transaction2.operation(id);


--
-- Name: operation_env_variable feky_operation_env_variable__operation; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation_env_variable
    ADD CONSTRAINT feky_operation_env_variable__operation FOREIGN KEY (operation_id) REFERENCES transaction2.operation(id);


--
-- Name: platform_env_variable feky_platform_env_variable__operation; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.platform_env_variable
    ADD CONSTRAINT feky_platform_env_variable__operation FOREIGN KEY (platform_id) REFERENCES transaction2.platform(id);


--
-- Name: base_transaction fkey_base_transaction__operation; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.base_transaction
    ADD CONSTRAINT fkey_base_transaction__operation FOREIGN KEY (operation_id) REFERENCES transaction2.operation(id);


--
-- Name: base_transaction fkey_base_transaction__transaction_status; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.base_transaction
    ADD CONSTRAINT fkey_base_transaction__transaction_status FOREIGN KEY (status_id) REFERENCES transaction2.transaction_status(id);


--
-- Name: file_based_transaction_setting fkey_file_based_tx_setting__file_setting_input; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_based_transaction_setting
    ADD CONSTRAINT fkey_file_based_tx_setting__file_setting_input FOREIGN KEY (input_file_setting_id) REFERENCES transaction2.file_setting(id);


--
-- Name: file_based_transaction_setting fkey_file_based_tx_setting__file_setting_output; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_based_transaction_setting
    ADD CONSTRAINT fkey_file_based_tx_setting__file_setting_output FOREIGN KEY (output_file_setting_id) REFERENCES transaction2.file_setting(id);


--
-- Name: file_based_transaction_setting fkey_file_based_tx_setting__operation; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_based_transaction_setting
    ADD CONSTRAINT fkey_file_based_tx_setting__operation FOREIGN KEY (operation_id) REFERENCES transaction2.operation(id);


--
-- Name: file_setting fkey_file_setting__section__footer_id; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_setting
    ADD CONSTRAINT fkey_file_setting__section__footer_id FOREIGN KEY (footer_id) REFERENCES transaction2.section(id);


--
-- Name: file_setting fkey_file_setting__section__header_id; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.file_setting
    ADD CONSTRAINT fkey_file_setting__section__header_id FOREIGN KEY (header_id) REFERENCES transaction2.section(id);


--
-- Name: inner_transaction fkey_inner_transaction__base_transaction; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.inner_transaction
    ADD CONSTRAINT fkey_inner_transaction__base_transaction FOREIGN KEY (parent_transaction_id) REFERENCES transaction2.base_transaction(id);


--
-- Name: inner_transaction fkey_inner_transaction__transaction_status; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.inner_transaction
    ADD CONSTRAINT fkey_inner_transaction__transaction_status FOREIGN KEY (status_id) REFERENCES transaction2.transaction_status(id);


--
-- Name: operation fkey_operation__platform__destination_id; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation
    ADD CONSTRAINT fkey_operation__platform__destination_id FOREIGN KEY (destination_id) REFERENCES transaction2.platform(id);


--
-- Name: operation fkey_operation__platform__source_id; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.operation
    ADD CONSTRAINT fkey_operation__platform__source_id FOREIGN KEY (source_id) REFERENCES transaction2.platform(id);


--
-- Name: section fkey_section__range; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.section
    ADD CONSTRAINT fkey_section__range FOREIGN KEY (range_id) REFERENCES transaction2.range(id);


--
-- Name: step_setting fkey_step_setting__file_based_transaction_setting; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.step_setting
    ADD CONSTRAINT fkey_step_setting__file_based_transaction_setting FOREIGN KEY (transaction_setting_id) REFERENCES transaction2.file_based_transaction_setting(id);


--
-- Name: step_setting fkey_step_setting__step_parameter; Type: FK CONSTRAINT; Schema: transaction2; Owner: admincloud
--

ALTER TABLE ONLY transaction2.step_setting
    ADD CONSTRAINT fkey_step_setting__step_parameter FOREIGN KEY (parameter_id) REFERENCES transaction2.step_parameter(id);


--
-- PostgreSQL database dump complete
--

